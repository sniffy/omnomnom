#!/bin/bash

nutch_version=2.3.1

rm -rf build
mkdir build
cd build
wget -O - http://www-eu.apache.org/dist/nutch/${nutch_version}/apache-nutch-${nutch_version}-src.tar.gz | tar xzvf - --strip-components 1
cp ../distconf/ivy/* ivy/
cp ../distconf/conf/* conf/
ant clean runtime
mkdir runtime/local/logs

cd ..
