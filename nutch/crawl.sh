#!/bin/bash

docker exec -i omnomnom_nomnomnutch_1 /data/nutch/local/bin/nutch generate -topN 100
docker exec -i omnomnom_nomnomnutch_1 /data/nutch/local/bin/nutch fetch -all
docker exec -i omnomnom_nomnomnutch_1 /data/nutch/local/bin/nutch parse -all
docker exec -i omnomnom_nomnomnutch_1 /data/nutch/local/bin/nutch updatedb -all
docker exec -i omnomnom_nomnomnutch_1 /data/nutch/local/bin/nutch index -all
