#!/bin/bash

nutchserver_port=$(printenv NUTCHSERVER_PORT)

/data/nutch/local/bin/nutch nutchserver $nutchserver_port
